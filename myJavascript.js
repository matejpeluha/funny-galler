var album = [];
let imageNumber = 7;
const gallery = document.getElementById("gallery");
const filterInput = document.getElementById("filter");
let draggedItem;
let hoveredItem;
let cookieAlbumPositions = [0, 1, 2, 3, 4, 5, 6];

$(document).ready(function() {
        imageNumber = 7;

        $.getJSON('images.json', function(jd) {
            for(let i = 0; i < imageNumber; i++) {
                let image = jd.photos[i];
                album.push(image);
            }


            sortThumbs();
            showImages();

            const oldImageNumber = getCookie("imageNumber");
            if(oldImageNumber)
                imageNumber = oldImageNumber;
            handleFilter();
        });


});

function sortThumbs(){
    const cookiePositions = getCookie("positions");
    let newPositions;

    if(cookiePositions){
        newPositions = cookiePositions.split(",").map(function(item) {
            return parseInt(item, 10);
        });
        cookieAlbumPositions = newPositions;
    }


    const tempAlbum = [];
    for(let i = 0; i < imageNumber; i++){
        tempAlbum[i] = album[cookieAlbumPositions[i]];
    }
    album = tempAlbum;
}


function showImages (){
    const thumbs = document.getElementsByClassName("thumb");

    for(let i = 0; i < imageNumber; i++){
        let thumb = thumbs[i];
        thumb.src = album[i].src;
        thumb.title = album[i].title;
        thumb.description = album[i].description;
        thumb.ondragstart = dragStart;
        thumb.ondragover = dragOver;
        thumb.style.display = "block";

        thumb.addEventListener("click", function (){
            const thumbs = document.getElementsByClassName("thumb");
            const carouselInner = document.getElementsByClassName("carousel-inner")[0];
            carouselInner.innerHTML = "";

            $('.carousel').carousel('pause');

            for(let index = 0; index < 7; index++) {
                if(thumbs[index].style.display === "block")
                    if(thumbs[index].id === thumb.id){
                        carouselInner.innerHTML += '<div class="item active">' +
                            ' <div>' +
                            '<img class="img-responsive" src="' + thumbs[index].src + '" alt="...">' +
                            '</div>' +
                            '<div class="cap">' +
                            '<div class="caption-text">' +
                            '<h4>' + thumbs[index].title + '</h4>' + '<span>' + thumbs[index].description + '</span>' +
                            '</div>' +
                            '<div class="pause-cycle-button">' +
                            '<button type="button" class="btn btn-primary btn-customized" onclick="handleCarousel()">' +
                            ' <span>' +
                            'AUTO-PLAY' +
                            '</span>' +
                            '</button>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }
                    else {
                        carouselInner.innerHTML += '<div class="item">' +
                            ' <div>' +
                            '<img class="img-responsive" src="' + thumbs[index].src + '" alt="...">' +
                            '</div>' +
                            '<div class="cap">' +
                            '<div class="caption-text">' +
                            '<h4>' + thumbs[index].title + '</h4>' + '<span>' + thumbs[index].description + '</span>' +
                            '</div>' +
                            '<div class="pause-cycle-button">' +
                            '<button type="button" class="btn btn-primary btn-customized" onclick="handleCarousel()">' +
                            ' <span>' +
                            'AUTO-PLAY' +
                            '</span>' +
                            '</button>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }
            }
        })
    }
}


function shuffle(){
    if(!draggedItem)
        return ;

    const thumbs = document.getElementsByClassName("thumb");

    let oldPosition = parseInt(draggedItem.id);

    for(let i = 0; i < imageNumber; i++){
        thumbs[i].id = "" + i;
    }

    const newPosition = parseInt(draggedItem.id);

    array_move(album, oldPosition, newPosition);
    array_move(cookieAlbumPositions, oldPosition, newPosition);

    setCookie("positions", cookieAlbumPositions);

    const images = document.getElementsByClassName("img-responsive");
    const imgDescription = document.getElementsByClassName("caption-text");


    for(let i = 0; i < imageNumber; i++){
        images[i].src = album[i].src;
        imgDescription[i].innerHTML = '<h4>' + album[i].title + '</h4>' + '<span>' + album[i].description +'</span>';
    }


}



$(window).load(function() {
    $('.carousel').carousel('pause');
});

function dragStart(evt){
    draggedItem = evt.target;
}

function dragOver(evt){
    hoveredItem = evt.target;
    const draggedId = parseInt(draggedItem.id);
    const hoveredId = parseInt(hoveredItem.id);

    if(draggedId >= hoveredId)
        gallery.insertBefore(draggedItem, hoveredItem);
    else
        gallery.insertBefore(draggedItem, hoveredItem.nextSibling);

    shuffle();
}


function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
}

function handleCarousel(){
    const carouselElement = $('.carousel');
    const buttons = document.getElementsByClassName('btn-customized');

    console.log(carouselElement);

    if(carouselElement.hasClass('pauseCarousel')) {
        carouselElement.removeClass("pauseCarousel");
        carouselElement.carousel('cycle');

        for(let i = 0; i < imageNumber; i++){
            buttons[i].innerHTML = '<span>' + 'STOP' + '</span>';
        }
    }
    else {
        carouselElement.addClass("pauseCarousel");
        carouselElement.carousel('pause');

        for(let i = 0; i < imageNumber; i++){
            buttons[i].innerHTML = '<span>' + 'AUTO-PLAY' + '</span>';
        }
    }
}

function handleFilter(){
    const filterInput = document.getElementById("filter");

    filterInput.addEventListener("input",filterImages);

    const cookieFilter = getCookie("input");
    if(cookieFilter){
        filterInput.value = cookieFilter;
        filterImages();
    }
}


function filterImages(){
    const thumbs = document.getElementsByClassName("thumb");


    const fullImageNumber = 7;
    imageNumber = 0;

    setCookie("input", filterInput.value, 1);


    const text = filterInput.value.toLowerCase();

    for(let i = 0; i < fullImageNumber; i++){
        const title = album[i].title.toLowerCase();
        const description = album[i].description.toLowerCase();
        if(title.includes(text) || description.includes(text)){
            thumbs[i].style.display = "block";
            imageNumber ++;
        }
        else{
            thumbs[i].style.display = "none";
        }

        setCookie("imageNumber", imageNumber, 1 );
    }
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}